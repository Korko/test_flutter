import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './widgets/counter.dart';
import './screens/home.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch:
            context.watch<Counter>().counter < 0 ? Colors.red : Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
