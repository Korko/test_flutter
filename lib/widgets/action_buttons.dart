import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './counter.dart';

class ActionButtons extends StatelessWidget {
  const ActionButtons({super.key});

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.vertical,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.all(10),
          child: FloatingActionButton(
            onPressed: context.read<Counter>().decrementCounter,
            tooltip: 'decrement',
            backgroundColor: Colors.white,
            child: const Icon(Icons.remove, color: Colors.black),
          ),
        ),
        Container(
          margin: const EdgeInsets.all(10),
          child: FloatingActionButton(
            onPressed: context.read<Counter>().incrementCounter,
            tooltip: 'Increment',
            child: const Icon(Icons.add),
          ),
        ),
      ],
    );
  }
}
