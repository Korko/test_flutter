import 'package:flutter/material.dart';

Future<void> confirm({
  required dynamic context,
  required String question,
  Widget? content,
  required void Function() onYes,
  void Function()? onNo,
  String yesText = "Yes",
  String noText = "No",
}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: (content == null) ? null : Text(question),
        content: (content == null) ? Text(question) : content,
        actions: <Widget>[
          TextButton(
            child: Text(yesText),
            onPressed: () {
              onYes();
              Navigator.of(context).pop();
            },
          ),
          TextButton(
            child: Text(noText),
            onPressed: () {
              if (onNo != null) {
                onNo();
              }
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
