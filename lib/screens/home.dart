import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../widgets/action_buttons.dart';
import '../widgets/counter.dart';
import '../utils/confirm.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (ctx, StateSetter setState) {
        return Scaffold(
          appBar: AppBar(
            title: Text(title),
          ),
          body: Column(children: [
            ElevatedButton(
              child: Text('Reset'),
              onPressed: () => confirm(
                  context: context,
                  question: 'Are you sure?',
                  onYes: () => context.read<Counter>().resetCounter()),
            ),
            Center(
              child: Text(
                'You have pushed the button ${context.watch<Counter>().counter} times',
                //style: Theme.of(context).textTheme.headline4,
              ),
            ),
          ]),
          floatingActionButton: const ActionButtons(),
        );
      },
    );
  }
}
