import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './widgets/counter.dart';
import './app.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<Counter>(create: (context) => Counter()),
    ],
    child: const MyApp(),
  ));
}
